package com.mysite.consumer.listener;

import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.Session;

import org.apache.activemq.command.ActiveMQTextMessage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.SessionAwareMessageListener;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.mysite.vo.MailVo;

/**
 * @描述: 队列监听器 .
 */
@Component
public class ConsumerSessionAwareMessageListener implements SessionAwareMessageListener<Message> {

	private static final Log log = LogFactory.getLog(ConsumerSessionAwareMessageListener.class);

	@Autowired
	private JmsTemplate activeMqJmsTemplate;
	@Autowired
	private Destination sessionAwareQueue;
	@Autowired
	private ThreadPoolTaskExecutor threadPool;

	public synchronized void onMessage(Message message, Session session) {
		try {
			ActiveMQTextMessage msg = (ActiveMQTextMessage) message;
			final String ms = msg.getText();
			log.info("==>receive message:" + ms);
			MailVo mailParam = JSONObject.parseObject(ms, MailVo.class);// 转换成相应的对象
			if (mailParam == null) {
				return;
			}

			try {
				sendEmail(mailParam);
			} catch (Exception e) {
				// 发送异常，重新放回队列
				// activeMqJmsTemplate.send(sessionAwareQueue, new MessageCreator() {
				// public Message createMessage(Session session) throws JMSException {
				// return session.createTextMessage(ms);
				// }
				// });
				log.error("==>MailException:", e);
			}
		} catch (Exception e) {
			log.error("==>", e);
		}
	}

	public void sendEmail(final MailVo vo) {
		threadPool.execute(new Runnable() {
			public void run() {
				try {
					System.err.println("执行具体业务  ------->> " + vo);
				} catch (Exception e) {
					throw e;
				}
			}
		});
	}
}
