package com.mysite.consumer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @描述: ActiveMQ测试启动类 .
 */
public class MQConsumerTest {
	private static final Log log = LogFactory.getLog(MQConsumerTest.class);

	public static void main(String[] args) {
		try {
			ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
					"classpath:spring-consumer.xml");
			context.start();

			while (true) {
				try {
					Thread.sleep(100000);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			log.error("==>MQ context start error:", e);
			System.exit(0);
		}
	}
}
