package com.mysite.producer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.mysite.producer.MQProducer;
import com.mysite.vo.MailVo;

/**
 * @描述: ActiveMQ测试启动类 .
 */
public class MQProducerTest {
	private static final Log log = LogFactory.getLog(MQProducerTest.class);

	public static void main(String[] args) {
		try {
			ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
					"classpath:spring-producer.xml");
			context.start();

			MQProducer mqProducer = (MQProducer) context.getBean("mqProducer");
			
			int count = 100;
			for (int i = 0; i < count; i++) {
				// 邮件发送
				MailVo mail = new MailVo();
				mail.setTo("iweiyi10@163.com");
				mail.setSubject("ActiveMQ测试");
				mail.setContent("通过ActiveMQ异步发送邮件！" + Math.random());
				mqProducer.sendMessage(mail);
				
				try {
					Thread.sleep(1000);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			context.stop();
		} catch (Exception e) {
			log.error("==>MQ context start error:", e);
			System.exit(0);
		} finally {
			log.info("===>System.exit");
			System.exit(0);
		}
	}
}
